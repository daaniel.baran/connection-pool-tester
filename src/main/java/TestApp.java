import java.sql.SQLException;
import java.util.Optional;

public class TestApp {

    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/client-server";
        String user = "daniel";
        String password = "passWord";
        String driver = "org.postgresql.Driver";

        ConnectionPool pool = new ConnectionPool(url, user, password, driver);
        pool.createPool();

        long start = System.currentTimeMillis();
        long end = start + 1 * 60_000; // 10 * 60sec = 1min

        String[] queries = {"select * from users", "select * from messages", "select * from ussers", "select * from messages where id>0", "select count(*) from users", "select count(*) from messages"};

        while (System.currentTimeMillis() < end) {

            int rand = 0 + (int) (Math.random() * queries.length);

            Optional<ConnectionNode> opt = pool.getConnectionInstance();

            if (opt.isPresent()) {
                ConnectionNode conn = opt.get();
                if (conn != null) {
                    int id = conn.getId();
                    try {
                        conn.getConnection().createStatement().executeQuery(queries[rand]);

                        pool.releaseConnection(conn);
                    } catch (SQLException e) {
//                        e.printStackTrace();
                        System.err.println("SQL EXCEPTION: CONNECTION-" + id);
                        conn.close();
                    }
                }
            }
        }
    }
}
